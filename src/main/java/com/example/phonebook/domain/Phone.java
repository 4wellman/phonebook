package com.example.phonebook.domain;

import javax.persistence.*;
import org.hibernate.validator.constraints.Length;

@Entity
public class Phone {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Length(max = 30, message = "Фамилия очень длинная (более 30 символов)")
    private String surname;
    @Length(max = 30, message = "Имя очень длинное (более 30 символов)")
    private String name;
    @Length(max = 30, message = "Отчество очень длинное (более 30 символов)")
    private String patronymic;
    @Length(max = 10, message = "Номер очень длинный (более 10 цифр)")
    private String worknum;
    @Length(max = 10, message = "Номер очень длинный (более 10 цифр)")
    private String mobilenum;
    @Length(max = 10, message = "Номер очень длинный (более 10 цифр)")
    private String homenum;

    public Phone() {
    }

    public Phone(String surname, String name, String patronymic, String worknum, String mobilenum, String homenum) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.worknum = worknum;
        this.mobilenum = mobilenum;
        this.homenum = homenum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getWorknum() {
        return worknum;
    }

    public void setWorknum(String worknum) {
        this.worknum = worknum;
    }

    public String getMobilenum() {
        return mobilenum;
    }

    public void setMobilenum(String mobilenum) {
        this.mobilenum = mobilenum;
    }

    public String getHomenum() {
        return homenum;
    }

    public void setHomenum(String homenum) {
        this.homenum = homenum;
    }
}
