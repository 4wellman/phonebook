package com.example.phonebook.controller;

import com.example.phonebook.domain.Phone;
import com.example.phonebook.repos.PhoneRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

@Controller
public class MainController {
    @Autowired
    private PhoneRepo phoneRepo;

    @GetMapping("/main")
    public String main(@RequestParam(required = false, defaultValue="") String filter, Model model) {
        Iterable<Phone> phones = phoneRepo.findAll();

        if( filter != null && !filter.isEmpty() ) {
            phones = phoneRepo.findBySurnameStartingWithOrNameStartingWithOrPatronymicStartingWithOrWorknumStartingWithOrMobilenumStartingWithOrHomenumStartingWith(filter,filter,filter,filter,filter,filter);
        } else {
            phones = phoneRepo.findAll();
        }

        model.addAttribute("phones", phones);
        model.addAttribute("filter", filter);

        return "main";
    }
    @PostMapping("/main")
    public String add(
            @Valid Phone phone,
            BindingResult bindingResult,
            Model model
    ) throws IOException {
        if (bindingResult.hasErrors()) {
            Map<String, String> errorsMap = ControllerUtils.getErrors(bindingResult);

            model.mergeAttributes(errorsMap);
            model.addAttribute("phone", phone);
        } else {
            model.addAttribute("phone", phone);
            phoneRepo.save(phone);
        }

        Iterable<Phone> phones = phoneRepo.findAll();
        model.addAttribute("phones", phones);

        return "main";
    }

    @GetMapping("/edit/{phone_id}")
    public String editForm(@PathVariable Long phone_id, Model model) {
        model.addAttribute("phone", phoneRepo.findById(phone_id) );

        return "edit";
    }

    @PostMapping(value = "/edit", params = "save")
    public String savePhone(
            @RequestParam String surname,
            @RequestParam String name,
            @RequestParam String patronymic,
            @RequestParam String worknum,
            @RequestParam String mobilenum,
            @RequestParam String homenum,
            @RequestParam("phone_id") Long phone_id
    ) {
        Phone phone = phoneRepo.findById(phone_id);
        phone.setSurname(surname);
        phone.setName(name);
        phone.setPatronymic(patronymic);
        phone.setWorknum(worknum);
        phone.setMobilenum(mobilenum);
        phone.setHomenum(homenum);

        phoneRepo.save(phone);

        return "redirect:/main";
    }

    @PostMapping(value = "/edit", params = "delete")
    public String delPhone( @RequestParam("phone_id") Long phone_id ) {
        Phone phone = phoneRepo.findById(phone_id);

        phoneRepo.delete(phone);

        return "redirect:/main";
    }
}