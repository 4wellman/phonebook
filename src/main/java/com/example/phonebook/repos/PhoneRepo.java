package com.example.phonebook.repos;

import com.example.phonebook.domain.Phone;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PhoneRepo extends CrudRepository<Phone, Integer> {
    Phone findById(Long id);
    List<Phone> findBySurnameStartingWithOrNameStartingWithOrPatronymicStartingWithOrWorknumStartingWithOrMobilenumStartingWithOrHomenumStartingWith(String surname, String name, String patronymic, String worknum, String mobilenum, String homenum);
    //StartingWith Containing
}
